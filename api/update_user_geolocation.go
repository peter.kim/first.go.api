package api

import (
	"log"
	"net/http"
	"time"

	"first.go.api/db"

	"github.com/gin-gonic/gin"
)

type userGeolocation struct {
	User      string  `json:"user" binding:"required"`
	Latitude  float64 `json:"latitude" binding:"required"`
	Longitude float64 `json:"longitude" binding:"required"`
}

type dbUserGeolocation struct {
	User           string `bson:"_id,omitempty"`
	Latitude       float64
	Longitude      float64
	LastUpdateTime time.Time
}

// standard Result format
type Result struct {
	Code    int
	Message string
	Fields  string
}

func (d *dbUserGeolocation) GetKey() string {
	return d.User
}

// update user geolocation
func UpdateUserGeolocation(c *gin.Context) {
	var req userGeolocation
	err := c.BindJSON(&req)
	if nil != err {
		log.Println(err)
		c.JSON(http.StatusBadRequest, Result{Code: http.StatusBadRequest, Message: "Invalid Request"})
		return
	}
	// update db
	data := dbUserGeolocation{User: req.User, Latitude: req.Latitude, Longitude: req.Longitude, LastUpdateTime: time.Now()}
	go db.InsertDB("FirstDB", "UserGeolocation", &data, db.Recover)
	c.JSON(http.StatusOK, Result{Code: http.StatusOK, Message: "Success"})
}
