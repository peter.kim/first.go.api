package db

import (
	"encoding/json"
	"fmt"
	"log"
	"os"
)

type dbConfig struct {
	Host string `json:"host"`
	Port uint   `json:"port"`
}

var config dbConfig

func init() {
	file, _ := os.Open("db.json")
	decoder := json.NewDecoder(file)
	err := decoder.Decode(&config)
	fmt.Printf("db.json / host : %s, port : %d\n", config.Host, config.Port)
	if nil != err {
		log.Fatal(err)
	}
}
