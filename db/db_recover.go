package db

import (
	"sync"
	"time"
)

type RecoverData struct {
	db, collection, key string
	data                KeyInterface
}

var (
	recoverMap map[string]RecoverData
	mu         sync.Mutex
	IsRunning  bool
	IsFinished bool
)

func init() {
	recoverMap = make(map[string]RecoverData)
	IsRunning = false
	IsFinished = true
	StartBGRoutine()
}

func addRecoverData(db, collection string, data *KeyInterface) {
	key := (*data).GetKey()
	if len(key) == 0 {
		return
	}

	recoverData := RecoverData{db, collection, key, *data}

	mu.Lock()
	recoverMap[key] = recoverData
	mu.Unlock()
}

func StartBGRoutine() bool {
	if !IsFinished {
		return false
	}

	IsRunning = true
	go checkAndRecover()
	return true
}

// check every 5 min
const checkDelaySecond time.Duration = 5 * time.Minute

func checkAndRecover() {
	for IsRunning {
		mu.Lock()
		key, data := get_some_key(recoverMap)
		// pop data
		if len(key) > 0 {
			delete(recoverMap, key)
		}
		mu.Unlock()

		if len(key) == 0 {
			time.Sleep(checkDelaySecond)
			continue
		}

		err := InsertDB(data.db, data.collection, data.data, Recover)
		if nil != err {
			mu.Lock()
			// if exists, skip
			if _, ok := recoverMap[key]; !ok {
				recoverMap[key] = *data
			}
			mu.Unlock()

			time.Sleep(checkDelaySecond)
			continue
		}
	}
	IsFinished = true
}

// get any key exists in a map.
func get_some_key(m map[string]RecoverData) (string, *RecoverData) {
	for k, d := range m {
		return k, &d
	}
	return "", nil
}
