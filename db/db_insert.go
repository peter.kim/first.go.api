package db

import (
	"fmt"

	"gopkg.in/mgo.v2"
)

type InsertType int

const (
	None InsertType = iota
	Recover
)

type KeyInterface interface {
	GetKey() string
}

func InsertDB(db, collection string, data KeyInterface, insertType InsertType) error {
	session, err := mgo.Dial(fmt.Sprintf("%s:%d", config.Host, config.Port))
	if nil != err {
		fmt.Println(err)
		if Recover == insertType {
			// recover
			addRecoverData(db, collection, &data)
			return nil
		}
		return err
	}

	// make session in safe mode
	session.SetSafe(&mgo.Safe{})
	_, err = session.DB(db).C(collection).UpsertId(data.GetKey(), &data)
	session.Close()

	if nil != err {
		fmt.Println(err)
		if Recover == insertType {
			// recover
			addRecoverData(db, collection, &data)
			return nil
		}
		return err
	}

	return nil
}
