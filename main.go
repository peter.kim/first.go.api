package main

import (
	"first.go.api/api"

	"github.com/gin-gonic/gin"
)

func main() {
	r := gin.Default()
	r.POST("/update_user_geolocation", api.UpdateUserGeolocation)
	r.StaticFile("/client", "html/client.html")
	r.Run(":8080")
}
